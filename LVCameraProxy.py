from time import time
from datetime import datetime
import numpy as np

from tango import DevState, AttrDataFormat, AttrWriteType
from tango.server import Device, attribute, command


class LVCameraProxy(Device):
    def init_device(self):
        super().init_device()
        self.set_state(DevState.ON)
        self.set_status("CameraProxy is ON")
        self._timestamp_raw: np.double
        self.dim_x = 1656  # 2304  Die Größe des Bildes muss genau passen.
        self.dim_y = 1492  # 1536    
        self._image_uint16_raw = np.zeros([self.dim_x, self.dim_y], dtype=np.uint16)
        self._image_uint16_processed = np.zeros([self.dim_x, self.dim_y], dtype=np.uint16)
        attr = attribute(name = "image_uint16_raw", dtype=np.uint16,    # Dekorator geht hier nicht, weil D. an den Klassenobjekten arbeiten,
                         max_dim_x = self.dim_x, max_dim_y=self.dim_y,  # nicht an den Instanzen. Das Attribut wird dynamisch angelegt.
                         dformat=AttrDataFormat.IMAGE,
                         fget=self.read_image_uint16_raw, 
                         fset=self.write_image_uint16_raw ) 
        self.add_attribute(attr)

        attr2 = attribute(name = "image_uint16_processed", dtype=np.uint16,    # Dekorator geht hier nicht, weil D. an den Klassenobjekten arbeiten,
                         max_dim_x = self.dim_x, max_dim_y=self.dim_y,  # nicht an den Instanzen. Das Attribut wird dynamisch angelegt.
                         dformat=AttrDataFormat.IMAGE,
                         fget=self.read_image_uint16_processed, 
                         fset=self.write_image_uint16_processed ) 
        self.add_attribute(attr2)

        # self.set_change_event("image_uint16_raw", True, False)
        self.set_change_event("image_uint16_processed", True, False)

    def read_image_uint16_raw(self, attr):
        return self._image_uint16_raw

    def write_image_uint16_raw(self, attr):
        self._image_uint16_raw = attr.get_write_value()
        # self.debug_stream(f"image_time: {attr.get_date().isoformat(sep=' ')}")
        # self.debug_stream(f"Linux time: {datetime.fromtimestamp(time())}")  
        try:
            dim_y, dim_x = self._image_uint16_raw.shape             # Es ist nicht klar, warum hier dim_x und dim_y verkehrt geliert werden...
            # self.debug_stream(f"Image Size X:={dim_x}, Y={dim_y}")  
            self.push_change_event("image_uint16_raw", self._image_uint16_raw, dim_x, dim_y)  # so funktioniert es mit ungenauen Größenangabe
        except Exception as e:
            self.debug_stream(f"Exception on push_change_event: {e}")
    
    def read_image_uint16_processed(self, attr):
        return self._image_uint16_processed

    def write_image_uint16_processed(self, attr):
        self._image_uint16_processed = attr.get_write_value()
        try:
            self.push_change_event("image_uint16_processed", self._image_uint16_processed, self.dim_x, self.dim_y)
        except Exception as e:
            self.debug_stream(f"Exception on push_change_event: {e}")

    @attribute(dtype=np.double, access=AttrWriteType.READ_WRITE)
    def timestamp_raw_image(self):
        return self._timestamp_raw
    
    @timestamp_raw_image.write
    def timestamp_raw_image(self, timestamp):
        self._timestamp_raw = timestamp
        self.debug_stream(f"Zeitdiffirenz Systemtime-LVTime: {time()-self._timestamp_raw}")
        # self.debug_stream(f"Windowssystemzeit: {datetime.fromtimestamp(timestamp)}")


if __name__ == "__main__":
    LVCameraProxy.run_server()