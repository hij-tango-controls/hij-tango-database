#als erstes im Terminal sudo pigpiod verwenden um Daemon zu starten
import pigpio
import time
pi=pigpio.pi()
pi.set_mode(6, pigpio.INPUT)
pi.set_mode(5, pigpio.INPUT)
pi.set_mode(22, pigpio.INPUT) #Verbindung (zu pigpio daemon) wird hergestellt und GPIOs 5,6 und 22 als Eingänge definiert.
count_five=0
count_six=0
count_twentytwo=0
count=0
#Variablen werden erstellt
def cbfa(gpio, level, tick): #3 Funktionen werden definiert, die später dafür sorgen, dass die Trigger gezählt werden.
    global count
    count+=1
    global count_five
    count_five+=1
def cbfb(gpio, level, tick):
    global count_six
    count_six+=1
    global count
    count+=1
    # print(f"Highflanke am GPIO 6, count = {count_six}")
def cbfc(gpio, level, tick):
    global count_twentytwo
    count_twentytwo+=1
    global count
    count+=1
timer=pi.get_current_tick()
#cb1=pi.callback(5, pigpio.RISING_EDGE, cbfa)
cb2=pi.callback(6, pigpio.RISING_EDGE, cbfb)
#cb3=pi.callback(22, pigpio.RISING_EDGE, cbfc) #für jeden Eingang wird ein callback gestartet, das bei jedem Signal (wenn die Spannung steigt (rising edge)) die Funktion danach ausführt. (cbfa, cbfb oder cbfc)
testtime = 2
print(f"zähle {testtime} sec")
time.sleep(testtime)
cb2.cancel()
pi.stop()
print(f"In {testtime} sec wurden {count_six} Impulse gezählt")

#print("t=", pi.get_current_tick()-timer, "total:", count, "twentytwo", count_twentytwo, "six:", count_six, "five:", count_five) #Ergebnis der Trigger-Zählung wird ausgegeben

