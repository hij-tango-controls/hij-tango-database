
import asyncio, threading, time
from tango import DevState, GreenMode, AttrWriteType, Util, AttrQuality
from tango.server import Device, command, attribute
from datetime import datetime
from LogManager import LogManager

import pyhdbpp

# Am RaspPI muss der pigpio(d) Deamon laufen, wird (sollte) per systemd beim Hochfahren automatisch gestartet.
import pigpio
GPIO_IP = '192.168.50.17'
LOG_INTERVAL = 50 # Sekunden

# Definition der Masken
BASE10Hz_MASK = 0b00000 
A1_MASK       = 0b00001
A2_MASK       = 0b00010
A3_MASK       = 0b00100
A4_MASK       = 0b01000
A5_MASK       = 0b10000

MASK_WAIT_TIME = 0.005 # Delay (s) das abgewartet wird, damit alle pigpio callbacks die Maske setzen können.

# Zuordnung der GPIO Pins
BASE10Hz_GPIO = 6
A1_GPIO       = 5
A2_GPIO       = 5
A3_GPIO       = 22
A4_GPIO       = 27
A5_GPIO       = 27

# Lege hier fest, ob 10Hz oder 1Hz Trigger zu ID Erzeugung (und Archivierung) genutzt wird.
MAIN_GPIO     = A1_GPIO # BASE10Hz_GPIO

TRIGGER_ATTR  = 'trigger_id'
MASK_ATTR     = 'amplifier_mask'

# Mapping von GPIO zu den entsprechenden Masken
GPIO_TO_MASKS = {
    BASE10Hz_GPIO: [BASE10Hz_MASK],
    A1_GPIO:       [A1_MASK, A2_MASK],
    A3_GPIO:       [A3_MASK],
    A4_GPIO:       [A4_MASK, A5_MASK]
}


class TriggerIDGenerator(Device):
    # green_mode = GreenMode.Asyncio

    def init_device(self):
        super().init_device()
        self.set_state(DevState.ON)
        self.count = 0
        self.tr_timestamp = 0
        self.amp_mask = 0
        self.db = Util.instance().get_database() # Zum Speichern des Triggercounters in Config DB
        self.set_change_event(TRIGGER_ATTR, True, False)
        self.set_archive_event(TRIGGER_ATTR, True, False)
        self.set_change_event(MASK_ATTR, True, False)
        self.set_archive_event(MASK_ATTR, True, False)
        self.amp_mask_lock = threading.Lock()
        self.log = LogManager()       # Öffnet die Logdatei
        self.read_last_trigger_id()   # Lese den letzen Wert von Tango HDBPP 
        loop = asyncio.get_event_loop()
        self.save_to_log_task = loop.create_task(self.periodic_check_and_save_counter(interval=LOG_INTERVAL))
        if not self.init_gpio():
            self.set_state(DevState.FAULT)
            
    def read_last_trigger_id(self):
        try:
            self.hdbpp = pyhdbpp.get_default_reader() # Muss in der Tango DB als free prop 
            self.trigger_path = (self.get_name() + '/' + TRIGGER_ATTR).lower()
            self.debug_stream(f"Trigger Attr Path: {self.trigger_path}")
            last_db_val= self.hdbpp.get_last_attribute_value(self.trigger_path)[2]
            if  self.log.trigger_id <= last_db_val:
                self.count = last_db_val
            else:
                self.error_stream(f"Something is wrong with hdbpp. \n Last trigger_id from logfile: {self.log.trigger_id} is bigger as value in hdb++ {last_db_val}")
                self.count = self.log.trigger_id + 1000   # Addiere ein Sicherheitsoffset dazu, damit keine Mehrdeutigkeiten bei Trigger_id entstehen

            self.debug_stream(f"Last trigger_id = {self.count}")
        except Exception as e:
            self.error_stream(f"Could not read trigger_id from hdbpp. Use the last from log file + 100. Exception: ")
            self.count = self.log.trigger_id + 1000   # Addiere ein Sicherheitsoffset dazu, damit keine Mehrdeutigkeiten bei Trigger_id entstehen
            self.debug_stream(f"Trigger from logfile: {self.log.trigger_id}, start count by: {self.count}")


    async def periodic_check_and_save_counter(self, interval: float):
        loop = asyncio.get_event_loop()
        while True:
            # save_counter ist eine synchrone Methode, deswegen run_in_executor
            await loop.run_in_executor(None, self.log.save_trigger_id, self.count, self.tr_timestamp)
            archived_id = await loop.run_in_executor(None, self.hdbpp.get_last_attribute_value, self.trigger_path)

            #überprüfe, ob der Wert aus der DB mindestens so hoch ist, wie der aus dem Log. Sonst ist etwas faul.
            if self.tr_timestamp - datetime.timestamp(archived_id[0]) > interval:
                self.error_stream(f"Last entry in HDBPP is too old! check hdb++es is runnung!")
            await asyncio.sleep(interval)


    def publish_ampifier_mask(self):
        self.debug_stream(f"publish amplifier mask = {self.amp_mask}")
        try:
            self._amplifier_mask = self.amp_mask
            self.push_change_event( MASK_ATTR, self._amplifier_mask, self.tr_timestamp, AttrQuality.ATTR_VALID)
            self.push_archive_event(MASK_ATTR, self._amplifier_mask, self.tr_timestamp, AttrQuality.ATTR_VALID)
            self.amp_mask = 0
        except Exception as e:
            self.error_stream(f"{e}")


    #async def delete_device(self):
    def delete_device(self):
        try:
            # stope GPIO callbacks und gebe Ressourcen frei
            for cb in self.callbacks: 
                cb.cancel()
            self.gpio.stop()
        except Exception as e:
            self.error_stream(f'Exception by stopping trigger gpio: {e}')
        try:
            self.log.save_trigger_id(self.count, self.tr_timestamp)
            self.log.close()
        except Exception as e:
            self.error_stream(f'Exception by writing trigger to file: {e}')
        try:
            self.save_to_log_task.cancel()
            # await self.save_to_log_task
            self.save_to_log_task
        except asyncio.CancelledError:
            print("Task wurde abgebrochen")  # Hier kommt eine Exception, weil der Task abgebrochen wird.

        super().delete_device()

    def gpio_trigger_cb(self, gpio_nr, level, tick):
        with self.amp_mask_lock:
            try: 
                if level == 1: # pigpio.RISING_EDGE:    
                    for mask in GPIO_TO_MASKS.get(gpio_nr):  # Abrufen der Masken, die diesem GPIO zugeordnet sind
                        self.amp_mask |= mask  
                    
                    if gpio_nr == MAIN_GPIO:             #  Wenn der Haupttrigger kommt:
                        self.count += 1                  #  -Erhöhe den Counter
                        self.tr_timestamp = time.time()  # - setze Timestamp, Publiziere den Wert
                        self.push_change_event( TRIGGER_ATTR, self.count, self.tr_timestamp, AttrQuality.ATTR_VALID)
                        self.push_archive_event(TRIGGER_ATTR, self.count, self.tr_timestamp, AttrQuality.ATTR_VALID)
                        t = threading.Timer(MASK_WAIT_TIME, self.publish_ampifier_mask ) # Muss leider immer neu erzeugt werden :-(
                        t.start()                        # - starte einen kurzen Timer, damit die Anderen Trigger verarbeitet werden können. 
            except Exception as e:
                self.error_stream(f"{e}")


    def init_gpio(self):
        try:
            self.gpio=pigpio.pi(GPIO_IP)
            if not self.gpio.connected:
                self.error_stream("No connection to GPIO Deamon. Check Deamon is running. Try sudo pigpiod")
                return False
            else:
                # Setzen der GPIO-Pins als Eingänge
                for gpio in GPIO_TO_MASKS.keys():
                    self.gpio.set_mode(gpio, pigpio.INPUT) #Verbindung (zu pigpio daemon) wird hergestellt und GPIOs 5,6 und 22 als Eingänge definiert.

                self.callbacks = []
                for gpio in GPIO_TO_MASKS.keys():
                    self.debug_stream(f"Add CB for gpio={gpio}, level = {pigpio.RISING_EDGE}")
                    cb = self.gpio.callback(gpio, pigpio.RISING_EDGE, self.gpio_trigger_cb)
                    self.callbacks.append(cb)
                return True
        except Exception as e:
            self.error_stream(e)
            return False
    
    @attribute(dtype=int,  access= AttrWriteType.READ)
    def trigger_id(self):
        return self.count
    

    @attribute(dtype=int)
    def amplifier_mask(self):
        return self._amplifier_mask

    @command
    def start(self):
        self.debug_stream("Start Kommando empfangen")
        if not self.task or self.task.cancelled():
            self.debug_stream("Starte Task.")
            self.task = asyncio.create_task(self._run())
        else:
            self.debug_stream("Trigger Task already running!")

    @command
    def stop(self):
        if self.task and not self.task.cancelled():
            self.debug_stream("Stoppe Task!")
            self.task.cancel()
        else:
            self.debug_stream("Task bereits gestoppt!")


if __name__ == "__main__":
    TriggerIDGenerator.run_server()