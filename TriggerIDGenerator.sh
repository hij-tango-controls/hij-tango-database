#!/bin/bash

# Print the parameters being used
echo "Running .py with the following parameters:"
for param in "$@"; do
    echo "$param"
done

# Execute the Python script with the provided parameters
python ShotIDGenerator/TriggerIDGenerator.py "$@" 
