import tango
from tango import EventType, EventData, AttrWriteType, AttrDataFormat, AttrQuality
from transitions.extensions.asyncio import AsyncMachine,  AsyncEventData
import time, datetime, json, asyncio, csv
from dataclasses import dataclass
import tracemalloc
tracemalloc.start()


WAITING = 'waiting'                         # Wait for Trigger or ProcessVariable Update
W_TRIGGER = 'w_trigger'                     # Wait for Trigger State
W_PVUPDATE = 'w_PVupdate'                   # Wait for PV
SAVING = 'saving'                           # Both PV and Trigger arrived, Save Data and go to first State
TIMEOUT = 'timeout_state'                   # PV or Trigger produce timout
TIMEDIFFOUTOFRANGE = 'timediff_out_of_range'    # Reagiere auf Situation, dass erlaubte zeitliche 
                                            # Abweichung zw. Trigger und PV überschritten wurde
                        



attr_data_format_dict = {enum_v.name: enum_v for _ , enum_v in AttrDataFormat.__dict__['values'].items()}   # Erzeugt {'SCALAR': tango._tango.AttrDataFormat.SCALAR, ..... }
attr_writetype_dict =   {enum_v.name: enum_v for _ , enum_v in AttrWriteType.__dict__['values'].items()}    # Erzeugt {'READ': tango._tango.AttrWriteType.READ, ....  }

@dataclass  # Erzeugt automatisch __init__, __eq__, __rep__
class AttrDef:
    '''
    The Dataclass for parsing of JSON attribute definition for add_dyn_attribute command
    '''
    name: str
    dtype: str = 'float'
    dformat: AttrDataFormat = AttrDataFormat.SCALAR
    writetype: AttrWriteType = AttrWriteType.READ_WRITE
    max_dim_x: int = 1
    timediff: float = 0.4
    trigger_timeout: float = 1.1    # für 1 Hz POLARIS Trigger
    pv_timeout: float = 1.1         # für 1 Hz ProzessVariable    


    @staticmethod
    def from_json(json_string: str) -> 'AttrDef':
        data = json.loads(json_string)
        return AttrDef(
            name = data['name'],
            dtype = data['dtype'],
            dformat = attr_data_format_dict[data.get('dformat', 'SCALAR')], #.get liefert dict wert, falls vorhanden oder die Alternative
            max_dim_x = data.get('max_dim_x', 1),
            writetype = attr_writetype_dict[data.get('writetype', 'READ_WRITE')],
            timediff = data.get("timediff", 0.1),
            trigger_timeout = data.get("trigger_timeout", 1.1),
            pv_timeout = data.get("pv_timeout",  1.1)                
        )
    

class PVWatcher:
    states = [WAITING, W_TRIGGER, W_PVUPDATE, SAVING, TIMEDIFFOUTOFRANGE, TIMEOUT]
    transitions= [
        { 'trigger': 'triggerID',       'source': WAITING,      'dest': W_PVUPDATE },
        { 'trigger': 'PV_update',       'source': WAITING,      'dest': W_TRIGGER},
        { 'trigger': 'triggerID',       'source': W_TRIGGER,    'dest': SAVING,             'conditions': 'is_timediff_in_range' },
        { 'trigger': 'triggerID',       'source': W_TRIGGER,    'dest': TIMEDIFFOUTOFRANGE,   'unless'    : 'is_timediff_in_range' },
        { 'trigger': 'PV_update',       'source': W_TRIGGER,    'dest': TIMEOUT,         },
        { 'trigger': 'PV_update',       'source': W_PVUPDATE,   'dest': SAVING,             'conditions': 'is_timediff_in_range'},
        { 'trigger': 'PV_update',       'source': W_PVUPDATE,   'dest': TIMEDIFFOUTOFRANGE,   'unless'    : 'is_timediff_in_range'},
        { 'trigger': 'triggerID',       'source': W_PVUPDATE,   'dest': TIMEOUT},
        { 'trigger': 'timeOut',         'source': '*',          'dest': TIMEOUT} 
    ]

    def __init__(self, device, attr_def : AttrDef) -> None:
        self.trigger_value=-1
        self.name = attr_def.name
        self.loop = device.loop
        self.device = device # Nur für Logging erforderlich.
        self.ds = device.debug_stream
        self.timeout_trigger = attr_def.trigger_timeout
        self.timeout_PVupdate = attr_def.pv_timeout
        self.timediff = attr_def.timediff
        self.trigger_timestamp = time.time()
        self.PV_update_timestamp = time.time()    
        self.sm=AsyncMachine(model = self, states=self.states, initial = WAITING, 
                             transitions = self.transitions, send_event=True, 
                             ignore_invalid_triggers=True) # Ignoriere Trigger stellt sich als wichtig heraus. Sonst gerät SM sporadisch in eine tote Schleife... Ursache ist nicht klar.
        
       
        # Zum Test mit Dateien  
        self.buffer = []
        self.buffer_limit = 60  # Anzahl der Einträge, nach denen der Puffer in die Datei geschrieben wird
        
    async def initialize(self):
        self.event_PV_update = asyncio.Event()
        self.event_tr_update = asyncio.Event()
        self.task_PV_event = self.loop.create_task( self.wait_for_PV_event()) 
        self.task_tr_event = self.loop.create_task( self.wait_for_tr_event() ) 
        self.trigger_AP = tango.AttributeProxy("POLARIS/TriggerIDGenerator/1/trigger_id")  # Austauschen durch Property
        self.evt_id_tr = self.trigger_AP.subscribe_event(EventType.CHANGE_EVENT, self.on_trigger_change )

    # Funktion zum Schreiben von Werten in den Puffer und bei Bedarf in die CSV-Datei
    def write_values_to_file(self):
        self.buffer.append((self.trigger_value, self.device._values[self.name])) # TriggerId und Wert
        
        # Wenn der Puffer die Grenze erreicht, in die Datei schreiben und Puffer leeren
        if len(self.buffer) >= self.buffer_limit:
            with open(self.name + '.csv', 'a', newline='') as file:
                writer = csv.writer(file)
                # Schreibe die gesammelten Werte in die CSV-Datei
                writer.writerows(self.buffer)
            self.buffer.clear()
            print(f"{self.buffer_limit} Werte in die CSV-Datei geschrieben.")

    def on_trigger_change(self, e: EventData ):
        self.trigger_value = e.attr_value.value
        try:
            self.trigger_timestamp = e.attr_value.time.totime()       # wandelt tango.TimeVal in Timestamp
        except Exception as e:
            print(e)
            
        self.loop.call_soon_threadsafe(self.event_tr_update.set) # Wird gesetzt, wenn Trigger_Id Change Event feuert.
                                                                 # Weil es außerhalb der Eventloop aufgerufen wird,
                                                                 # muss man mit call_soon_treadsafe aufrufen. 
 

    async def wait_for_tr_event(self):
        '''
            wait_for_tr_event() should be started in a taks and waits for an event
            until the task will be canceled.
            Sonst könnte das timeout nicht registriert werden.
        '''
        while True:
            try:
                if self.event_tr_update.is_set():           # Check, ob das Event bereits gesetzt ist
                    self.ds(f"PV {self.name}: Trigger-Event war bereits gesetzt, bevor gewartet wurde.")
                    self.event_tr_update.clear()            # Setzt das Event zurück, um den nächsten Zyklus zu ermöglichen
                    continue                               
                await asyncio.wait_for(self.event_tr_update.wait(), self.timeout_trigger)
                # self.trigger_timestamp = time.time()   # wird in on_trigger_change gesetzt.
                await self.triggerID()
                self.event_tr_update.clear()  # Wichtig! Event zurücksetzen, damit das Warten erneut beginnt.
            except asyncio.TimeoutError:
                self.ds(f"PV {self.name}: Timout waiting for Trigger")
                await self.timeOut()
            except Exception as e:
                self.error_stream(f"Exception in wait_for_tr_event: {e}")
    
    def on_PV_change(self):
        # self.loop.call_soon_threadsafe(self.event_PV_update.set)
        self.event_PV_update.set() 

    async def wait_for_PV_event(self):
        '''
            wait_for_PV_event() should be started in a taks and waits for an event
        '''
        while True:
            # self.ds(f'PV event loop')
            # Check, ob das Event bereits gesetzt ist
            if self.event_PV_update.is_set():
                self.ds(f"PV {self.name}: PV-Event war bereits gesetzt, bevor gewartet wurde.")
                self.event_PV_update.clear()            # Setzt das Event zurück, um den nächsten Zyklus zu ermöglichen
                continue                                
            try:
                # self.ds("warte auf event_PV_update event")
                await asyncio.wait_for(self.event_PV_update.wait(), self.timeout_PVupdate)
                self.PV_update_timestamp = time.time()
                # self.ds('PV update empfangen')
                await self.PV_update()
                self.event_PV_update.clear()  # Wichtig! Event zurücksetzen, damit das Warten erneut beginnt.
            except asyncio.TimeoutError as e:
                self.ds(f"PV {self.name}: Timout waiting for PV Update")
                await self.timeOut()    
            except Exception as e:
                self.error_stream(f"Exception in wait_for_PV_event: {e}")

    async def is_timediff_in_range(self, eventdata: AsyncEventData)->bool:
        diff = abs(self.trigger_timestamp - self.PV_update_timestamp)
        # self.ds(f"Zeitdiff: {diff}, TIMEDIFF in Range= {diff < self.timediff}")
        return diff < self.timediff
        # return True

    async def on_enter_timediff_out_of_range(self, eventdata: AsyncEventData):
        # self.ds("Zustand: TIMEDIFF out of Range")
        await self.to_waiting()

    async def on_enter_saving(self, eventdata: AsyncEventData):
        try: 
            ts = datetime.datetime.fromtimestamp(self.trigger_timestamp).strftime("%Y-%m-%d %H:%M:%S.%f")
            self.ds(f"PV {self.name}: Zustand: saving, timestamp: {ts}")
        except Exception as e:
            print(e)
        
        self.loop.call_soon_threadsafe(self.device.push_archive_event, self.name, self.device._values[self.name] , self.trigger_timestamp, AttrQuality.ATTR_VALID) # geht

        # -------- Missbrauche value_r um TriggerId zu speichern:
        # self.loop.call_soon_threadsafe(self.device.push_archive_event, self.name, self.trigger_value, self.trigger_timestamp, AttrQuality.ATTR_VALID)
        # --------- 

        # self.write_values_to_file()
        await self.to_waiting()

    async def on_enter_waiting(self, eventdata: AsyncEventData):
        self.ds(f"PV {self.name}: Zustand: waiting")
        pass
    

    async def on_enter_timeout_state(self, eventdata: AsyncEventData):
        self.ds(f"PV {self.name}: Zustand: Timeout")
        await self.to_waiting()

    async def stop(self):
        self.ds(f"Beende pvWatcher für {self.name}")
        try:
            self.trigger_AP.unsubscribe_event( self.evt_id_tr ) # Müsste abgemeldet werden. 
                                                            # aber dann bricht die Ausführung ab.
        except Exception as e:
             self.ds(f"Error unsubscribing event for {self.name}: {e}")

        self.ds(f"PV {self.name}: cancel events")                                                  
        self.task_PV_event.cancel()
        self.task_tr_event.cancel()
        try:
            await self.task_PV_event
        except asyncio.CancelledError:
            self.ds(f"Task PV event cancelled successfully for {self.name}")
        try:
            await self.task_tr_event
        except asyncio.CancelledError:
            self.ds(f"Task TR event cancelled successfully for {self.name}")

        self.ds(f"PV {self.name}: beendet.")
        

