import json
from datetime import datetime
import os

class LogManager:
    def __init__(self, log_file='TriggerIDGenerator.log'):
        """
        Initialisiert den LogManager, erstellt einen neuen Log-entry mit Startzeit,
        aktuellem trigger_id und Maske.

        Args:
            log_file (str): Pfad zur Log-Datei.
            maske (str): Wert für das Feld "Maske".
        """
        self.log_file = log_file
        self.startzeit = datetime.now()
        self.stopzeit = None
        self.trigger_id = self._get_last_trigger_id()
        self.entry = {
            "Started at": self.startzeit.strftime("%Y-%m-%d %H:%M:%S.%f"),
            "Stopped at": "",
            "trigger_id": self.trigger_id,
            "timestamp" : "", 
        }
        self._get_last_trigger_id()
        self._create_new_entry()

    def _get_last_trigger_id(self):
        """
        Liest den letzten trigger_id-Wert aus der Log-Datei.

        Returns:
            int: Der nächste trigger_id-Wert.
        """
        if not os.path.isfile(self.log_file):
            print(f"Datei nicht vorhanden: {self.log_file}")
            return 0
        with open(self.log_file, 'r', encoding='utf-8') as f:
            try:
                daten = json.load(f)
                if daten:
                    last_entry = daten[0]
                    return last_entry.get("trigger_id", 0)
                else:
                    return 0
            except json.JSONDecodeError:
                print("JSON Decode error")
                return 0

    def _create_new_entry(self):
        """
        Fügt den neuen entry zur Log-Datei hinzu.
        """
        if not os.path.isfile(self.log_file):
            with open(self.log_file, 'w', encoding='utf-8') as f:
                json.dump([self.entry], f, ensure_ascii=False, indent=4)
        else:
            with open(self.log_file, 'r+', encoding='utf-8') as f:
                try:
                    daten = json.load(f)
                except json.JSONDecodeError:
                    daten = []
                daten.insert(0, self.entry)
                f.seek(0)
                json.dump(daten, f, ensure_ascii=False, indent=4)

    def save_trigger_id(self, new_trigger_id, timestamp):
        """
        Aktualisiert den trigger_id des aktuellen entrys.

        Args:
            new_trigger_id (int): Neuer Wert für den trigger_id.
        """
        self.trigger_id = new_trigger_id
        self.entry['trigger_id'] = self.trigger_id
        self.entry['timestamp'] = datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        self._update_last_entry()

    def _update_last_entry(self):
        """
        Aktualisiert den letzten entry in der Log-Datei mit den aktuellen Daten.
        """
        if not os.path.isfile(self.log_file):
            # Wenn die Datei nicht existiert, nichts tun
            return
        with open(self.log_file, 'r+', encoding='utf-8') as f:
            try:
                daten = json.load(f)
            except json.JSONDecodeError:
                daten = []
            if daten:
                daten[0] = self.entry
                f.seek(0)
                json.dump(daten, f, ensure_ascii=False, indent=4)
                f.truncate()

    def close(self):
        """
        Setzt die Stopzeit und aktualisiert den letzten entry in der Log-Datei.
        """
        self.stopzeit = datetime.now()
        self.entry['Stopped at'] = self.stopzeit.strftime("%Y-%m-%d %H:%M:%S.%f")
        self._update_last_entry()

    def __enter__(self):
        """
        Ermöglicht die Nutzung des LogManagers mit der 'with'-Anweisung.
        """
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Stellt sicher, dass die Stopzeit gesetzt wird, wenn der Kontext verlassen wird.
        """
        self.close()

 