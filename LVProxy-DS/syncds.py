import asyncio
import tango
from tango.server import Device, attribute, command
from threading import Thread

def side_thread(loop):
    asyncio.set_event_loop(loop)
    loop.run_forever()

class SyncDS(Device):
    def init_device(self):
        super().init_device()
        self.loop = asyncio.new_event_loop()
        # --- ACTIVATE DEBUG MODE ---
        self.loop.set_debug(True)
        #-------------------------
        self.thread = Thread(target=side_thread, args=(self.loop,), daemon=True, name="AsyncThread")
        self.thread.start()
        self.myAsyncLookForTheAnswer = AsyncLookForTheAnswer("async_part", self)  # Important to pass a reference of this object to myAsyncLookForTheAnswer
        self.answer = 0

    def sync_method_for_answer(self, value):
        print(f"A sync method was called from async code with answer = {value}")
        self.answer = value
    
    @command
    def calculate_the_answer(self):
        print("Start time-consuming task asynchronously")
        # pass the event loop :
        asyncio.run_coroutine_threadsafe(self.myAsyncLookForTheAnswer.calculate_the_answer(), self.loop) # Don't call directly!
        print("continue with my synchronous jobs")

    @attribute(dtype=int)
    def the_answer(self):
        return self.answer

# ------   THE ASYNC PART -------------
class AsyncLookForTheAnswer:
    def __init__(self, name: str, device) -> None:
        self.name = name
        self.loop = device.loop
        self.device = device

    async def calculate_the_answer(self):
        await asyncio.sleep(5)
        # Call the sync method in a thread-safe manner from the event loop
        self.loop.call_soon_threadsafe(self.device.sync_method_for_answer, 42) # Don't call directly!

if __name__ == "__main__":
    dev_info = tango.DbDevInfo()
    dev_info.server = "SyncDS/test"
    dev_info._class = "SyncDS"
    dev_info.name = "test/syncDS/1"
    db = tango.Database()
    db.add_device(dev_info)

    SyncDS.run_server()