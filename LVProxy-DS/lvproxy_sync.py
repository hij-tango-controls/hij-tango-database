import time, tango
from tango.server import Device, attribute
from tango import AttrQuality as q    
from tango import DevState, GreenMode

import tracemalloc
tracemalloc.start()

class LVProxySync(Device):
    #green_mode = GreenMode.Asyncio

    def init_device(self):
        super().init_device()
        self._sinus_signal=float('nan')
        self._saw_signal=float('nan')
        self.set_change_event('sinus_signal', True, False)
        self.set_change_event('saw_signal', True, False)
        self.set_state(DevState.RUNNING)

    @attribute(dtype=float)
    def sinus_signal(self):
        return self._sinus_signal

    @sinus_signal.write
    def set_sinus_signal(self, nValue):
        self.push_change_event("sinus_signal", nValue, time.time() , q.ATTR_VALID)
        self._sinus_signal = nValue

    @attribute(dtype=float)
    def saw_signal(self):
        return self._saw_signal

    @saw_signal.write
    def set_saw_signal(self, nValue):
        self.push_change_event("saw_signal", nValue, time.time() , q.ATTR_VALID)
        self._saw_signal = nValue


if __name__ == "__main__":
    dev_info = tango.DbDevInfo()
    dev_info.server = "lvproxysync/lvproxysync"
    dev_info._class = "lvproxysync"
    dev_info.name = "lvproxysync/lvproxysync/1"
    db = tango.Database()
    db.add_device(dev_info)
    LVProxySync.run_server()
