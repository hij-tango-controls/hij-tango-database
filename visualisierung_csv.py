import numpy as np
import matplotlib.pyplot as plt
import argparse

# Funktion zum Einlesen der CSV-Datei und Visualisierung der x- und y-Werte mit NumPy
def plot_xy_from_csv(file_path, max_points = 1000):
    # Lese die CSV-Datei ein, überspringe die Kopfzeile und lade die Daten
    data = np.loadtxt(file_path, delimiter=',', skiprows=1)

    # Extrahiere x- und y-Werte (erste und zweite Spalte)
    x_values = data[:, 0]
    y_values = data[:, 1]
    if len(x_values) > max_points:
        x_values = x_values[-max_points:]
        y_values = y_values[-max_points:]
    
    fig = plt.figure(figsize=(10, 6))  # Nur ein figure()-Aufruf
    fig.canvas.manager.set_window_title(file_path)  # Fenster-Titel setzen
    plt.plot(x_values, y_values, marker='o', linestyle='-', color='b')  # Liniendiagramm mit Punkten
    plt.xlabel('X-Werte')  # Beschriftung der x-Achse
    plt.ylabel('Y-Werte')  # Beschriftung der y-Achse
    # plt.title('Visualisierung der X- und Y-Werte aus CSV mit NumPy')  # Titel des Diagramms
    plt.grid(True)  # Gitternetz anzeigen
    plt.minorticks_on()  # Kleine Ticks anzeigen
    plt.show()  # Diagramm anzeigen

# Pfad zur CSV-Datei
csv_file_path = 'example.csv'  # Passe den Pfad zur Datei an



# Main-Funktion zur Verarbeitung der Konsolenargumente
def main():
    parser = argparse.ArgumentParser(description='Visualisierung von X- und Y-Werten aus einer CSV-Datei')
    parser.add_argument('file_path', type=str, help='Pfad zur CSV-Datei')
    parser.add_argument('--max_points', type=int, default=None, help='Maximale Anzahl der anzuzeigenden Werte (optional)')

    args = parser.parse_args()

    # Aufruf der Plot-Funktion mit den übergebenen Argumenten
    plot_xy_from_csv(args.file_path, args.max_points)

if __name__ == "__main__":
    main()
