import time, asyncio, tango
from tango.server import Device, attribute
from tango import AttrQuality as q    
from tango import DevState, GreenMode
import tracemalloc
tracemalloc.start()

class LVProxyAsync(Device):
    green_mode = GreenMode.Asyncio
    
    async def init_device(self):
        super().init_device()
        self._sinus_signal=float('nan')
        self._saw_signal = float('nan')
        self.set_change_event('sinus_signal', True, False)
        self.set_change_event('saw_signal', True, False)
        self.set_state(DevState.RUNNING)
        self._values={}

    @attribute(dtype=float)
    async def sinus_signal(self):
        return self._sinus_signal

    @sinus_signal.write
    async def set_sinus_signal(self, nValue):
        self.debug_stream(f"Write Sinus: {nValue}")
        self._sinus_signal = nValue
        self.push_change_event("sinus_signal", nValue, time.time() , q.ATTR_VALID)
        #self.sinus_Watcher.event_PV_update.set() # Signalisiere Wertänderung

    @attribute(dtype=float)
    async def saw_signal(self):
        return self._saw_signal

    @saw_signal.write
    async def set_saw_signal(self, nValue):
        self._saw_signal=nValue
        self.push_change_event("saw_signal", nValue, time.time() , q.ATTR_VALID)

if __name__ == "__main__":
    dev_info = tango.DbDevInfo()
    dev_info.server = "lvproxyasync/lvproxyasync"
    dev_info._class = "lvproxyasync"
    dev_info.name = "lvproxyasync/lvproxyasync/1"
    db = tango.Database()
    db.add_device(dev_info)
    LVProxyAsync.run_server()
