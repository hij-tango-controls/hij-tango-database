import time, asyncio, threading
from tango.server import Device, attribute, command
from tango import AttrQuality as q    
from tango import DevState, Database
from PVWatcher import PVWatcher, AttrDef
from threading import Thread

import tracemalloc
tracemalloc.start()

import sys

def handle_exception(exc_type, exc_value, exc_traceback):
    # Überspringe KeyboardInterrupt, um das Programm nicht zu beenden, wenn Ctrl+C verwendet wird
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    
    # Standardbehandlung für nicht abgefangene Ausnahmen, gefolgt von Programmbeendigung
    print("Uncaught exception:", exc_value)
    sys.exit(1)

# Setze den benutzerdefinierten Exception-Handler
sys.excepthook = handle_exception


def thread_exception_handler(args):
    print(f"Exception in thread {args.thread.name}: {args.exc_value}")
    sys.exit(1)

print("Setze den benutzerdefinierten Exception-Handler für Threads")
threading.excepthook = thread_exception_handler

def handle_async_exception(loop, context):
    # Kontext enthält Informationen zur Ausnahme
    msg = context.get("exception", context["message"])
    print(f"Caught async exception: {msg}")
    loop.stop()  # Stoppe den Event-Loop
    sys.exit(1)


def side_thread(loop):
    asyncio.set_event_loop(loop)
    loop.run_forever()

class LVProxy(Device):
    def init_device(self):
        super().init_device()
        self.ds = self.debug_stream

        self.loop = asyncio.new_event_loop()
        self.loop.set_exception_handler(handle_async_exception)
        #------
        self.loop.set_debug(True)   # DEBUG MODUS!!!
        #-----
        self.thread = Thread(target=side_thread, args=(self.loop,), daemon=True, name="LVProxy thread für PVWatcher")
        self.thread.start()

        self.set_state(DevState.RUNNING)
        self._dyn_attrs={}
        self._values = {}
        db = Database()
        pv_props = db.get_device_property_list(self.get_name(), "pv_*").value_string
        for p in pv_props:
            try:
                devprop = db.get_device_property(self.get_name(), p)[p]
                json_str = devprop[0] # Extrahiere str aus {'pv_demo_spectrum': ['JSONstr']}
                attrdef=AttrDef.from_json(json_str)
            except Exception as e:
                self.debug_stream(f"Property {p} was probably edited by hand, try to parse")
                try:
                    # Falls property in jive editiert wurde, bekommt man ein String in der Form: 
                    # [' ', '            {', '                "name": "demo_spectrum", ', '   ....
                    # also ein Array aus Strings. Bau daraus wieder ein hübsches JSON.
                    json_str = '\n'.join(devprop) 
                    attrdef=AttrDef.from_json(json_str)
                except Exception as e:
                    self.error_stream(f"Error on parsing Attribute {p}, Exception: {e}")
                    continue
            try:
                self.create_attr(attrdef, is_calibration_attr=False)
            except Exception as e:
                self.error_stream(f"Error on create Attribute {p}, Exception: {e}")

                
        cal_props = db.get_device_property_list(self.get_name(), "cal_*").value_string
        for c in cal_props:
            try:
                json_str = db.get_device_property(self.get_name(), c)[c][0]
                self.create_attr(AttrDef.from_json(json_str),is_calibration_attr=True)
            except Exception as e:
                self.error_stream(f"Error on create Attribute {p}, Exception: {e}")


    def delete_device(self):
        for key, a in self._dyn_attrs.items():
            try:
                if not a['is_calibration']:
                    self.ds(f"sende stop zu {key} ")
                    f = asyncio.run_coroutine_threadsafe(a['pv_watcher'].stop(), self.loop)
                    f.result(timeout=0.1)
            except Exception as e:
                self.error_stream(f"Error stopping attribute {key} {a}: {e}")
        try:
            if self.loop.is_running():
                self.ds(f"beende loop.")
                self.loop.call_soon_threadsafe(self.loop.stop)       
        except Exception as e:
            self.error_stream(f"Problem bei self.loop.is_running() : {e}") 
    
        # Warte auf den Thread, um sicherzustellen, dass er sauber beendet wird
        self.ds("Waiting for thread to finish.")
        if self.thread.is_alive():
            self.thread.join()
        
        self.ds("Thread and loop have been stopped.")

        # time.sleep(2)
        super().delete_device()

        
    def create_attr(self, attr_def, is_calibration_attr = False):
        attr_name = attr_def.name
        if attr_name not in self._dyn_attrs:
            attr = attribute(
                name=attr_name,
                dtype=attr_def.dtype,
                access=attr_def.writetype,
                dformat = attr_def.dformat,
                max_dim_x = attr_def.max_dim_x,
                fget=self.generic_read,
                fset=self.generic_write,
            )
            self.add_attribute(attr)                        # Tango, füge dyn. Attribut hinzu
            self.set_change_event(attr_name, True, False)   # Festlegen, dass Change und 
            self.set_archive_event(attr_name, True, False)  # und Archive Events vom Code gesetzt werden
            if not is_calibration_attr:                     # Falls normaler Attribut: starte Watchdog SM
                pvw = PVWatcher(self, attr_def)             
                f = asyncio.run_coroutine_threadsafe(pvw.initialize(), self.loop)
                self._dyn_attrs[attr_name] = {'pv_watcher' : pvw, 'future': f, 'is_calibration': False}
            else:
                self._dyn_attrs[attr_name] = {'is_calibration': True}

            self._values[attr_name] = None
        else:
            raise ValueError(f"Already have an attribute called {repr(attr_name)}")


    @command(dtype_in=str)
    def add_pv_attr(self, attr_str):
        '''
        add_pv_attr takes a attribute defination in json format:
        attr_json = 
            {
                "name": "example_attribute", 
                "dtype": "float",           (see https://pytango.readthedocs.io/en/latest/api/server_api/server.html#pytango-hlapi-datatypes, 'int16', etc.)
                "dformat": "SCALAR",        (optional, SCALAR (default); SPECTRUM, IMAGE)
                "writetype": "READ_WRITE"   (optional)
                "max_dim_x" : 1             (for SPECTRUM Attr)
                "timediff" = 0.1,           (maximal temporal diffirenz between new trigger id and process variable)
                "trigger_timeout" = 0.1,
                "pv_timeout" = 0.1
            }
        '''
        attr_def = AttrDef.from_json(attr_str) # Falls nicht klappt, wird JSONDecodeError Exception ausgelöst.
        self.create_attr(attr_def)
        attr_name = attr_def.name
        db = Database()
        db.put_device_property( self.get_name(), {f"pv_{attr_name}": attr_str } )
        self.info_stream("Added dynamic attribute %r", attr_name)
    
    @command(dtype_in=str)
    def add_cal_attr(self, attr_str):
        '''
        add_cal_attr adds an calibration attribute, witch changes seldom.
        takes a attribute defination in json format:
        attr_json = 
            {
                "name": "calibration_attribute", 
                "dtype": "float",           (see https://pytango.readthedocs.io/en/latest/api/server_api/server.html#pytango-hlapi-datatypes, 'int16', etc.)
                "dformat": "SPECTRUM",      (optional, SCALAR (default); SPECTRUM, IMAGE)
                "writetype": "READ_WRITE"   (optional)
                "max_dim_x" : 256             
            }
        '''
        attr_def = AttrDef.from_json(attr_str) # Falls nicht klappt, wird JSONDecodeError Exception ausgelöst.
        self.create_attr(attr_def, is_calibration_attr=True)
        attr_name = attr_def.name
        db = Database()
        db.put_device_property( self.get_name(), {f"cal_{attr_name}": attr_str } )
        self.info_stream("Added calibration attribute %r", attr_name)
    
       
    def generic_read(self, attr):
        value = self._values[attr.get_name()]
        return value

    def generic_write(self, attr):
        attr_name = attr.get_name()
        value = attr.get_write_value()
        # self.debug_stream("Writing attribute %s - value %s", attr_name, value)
        self._values[attr_name] = value
        self.push_change_event(attr_name, value, time.time() , q.ATTR_VALID)
        try:
            if not self._dyn_attrs[attr_name]['is_calibration']: # für Calibrierungsatribute gibt es keinen Abgleich zu Trigger.
                self.ds(f"aktualisiere watchdog {attr_name}")
                self.loop.call_soon_threadsafe(self._dyn_attrs[attr_name]['pv_watcher'].on_PV_change)
            else:
                self.ds(f"Write calibration attribute {attr_name}")
        except Exception as e:
            self.ds(f"Fehler beim Schreiben: {e}")


if __name__ == "__main__":
    try:
        LVProxy.run_server()
    except Exception as e:
        print(f"Unhandled exception: {e}")
        sys.exit(1)
