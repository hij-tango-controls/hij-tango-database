
import asyncio
from tango import DevState, GreenMode, AttrWriteType, Util, AttrQuality
from tango.server import Device, command, attribute
import time as t
import statistics as st

A1_MASK = 0b00001
A2_MASK = 0b00010
A3_MASK = 0b00100
A4_MASK = 0b01000
A5_MASK = 0b10000
ATTRNAME = 'trigger_id'

class TriggerIDGenerator(Device):
    green_mode = GreenMode.Asyncio

    async def init_device(self):
        await super().init_device()
        self.set_state(DevState.ON)
        self.count = 0
        self.task : asyncio.Task = None
        self.ampfilierOn = A1_MASK | A2_MASK
        self.initialized = False
        self._freq : float = 42.0
        self.db = Util.instance().get_database() # Zum Speichern des Triggercounters in Config DB
        self.set_change_event(ATTRNAME, True, False)
        self.set_archive_event(ATTRNAME, True, False)
        
    
    @attribute(unit="Hz", dtype=float, memorized=True, hw_memorized = True, access= AttrWriteType.READ_WRITE)
    async def freq(self):
        return self._freq
    
    @freq.write
    async def set_freq(self, fr):
        self._freq = fr
        self.interval = 1/fr 
        self.debug_stream(f"Write Interval = {self.interval}")
        await self.start()

    @attribute(dtype=int, memorized=True, hw_memorized = True, access= AttrWriteType.READ_WRITE)
    async def trigger_id(self):
        return self.count
    
    @trigger_id.write
    def set_trigger_id(self, c):
        if not self.initialized:
            self.debug_stream(f"Write trigger ID dummy c= {c}")
            self.initialized = True
            self.count = c

    @attribute(dtype=int)
    async def ammplifier_mask(self):
        return self.ampfilierOn

    @command
    async def start(self):
        self.debug_stream("Start Kommando empfangen")
        if not self.task or self.task.cancelled():
            self.debug_stream("Starte Task.")
            self.task = asyncio.create_task(self._run())
        else:
            self.debug_stream("Trigger Task already running!")

    @command
    async def stop(self):
        if self.task and not self.task.cancelled():
            self.debug_stream("Stoppe Task!")
            self.task.cancel()
        else:
            self.debug_stream("Task bereits gestoppt!")

    async def _run(self):
        self.debug_stream(f'Enter _run, Interval = {self.interval}')
        while True:
            await asyncio.sleep(self.interval)
            self.count += 1
            self.trigger = self.count
            timestamp = t.time()
            self.push_change_event( ATTRNAME, self.count, timestamp, AttrQuality.ATTR_VALID)
            self.push_archive_event(ATTRNAME, self.count, timestamp, AttrQuality.ATTR_VALID)
            self.ampfilierOn = A1_MASK | A2_MASK
            if self.count % 10 == 0:
                self.ampfilierOn |= A3_MASK
            if self.count % 50 == 0:
                self.ampfilierOn |= A4_MASK | A5_MASK
                await self.save_trigger_id_in_db()

    async def save_trigger_id_in_db(self):
        '''
            Speichert den wert der trigger_id in der Tango config DB
        '''
        name = self.get_name()
        attr = "trigger_id"
        self.db.put_device_attribute_property(name, {attr: {"__value": self.count}}) 

if __name__ == "__main__":
    TriggerIDGenerator.run_server()