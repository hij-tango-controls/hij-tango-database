#first use sudo pigpiod [sample rate (1-10 us; 1 highest, 10 lowest, default 5)]
import pigpio
import time
import json
import datetime
#timer for test runs
timer = 100
#set the GPIOs up as input
pi = pigpio.pi()
pi.set_mode(5, pigpio.INPUT)
pi.set_mode(6, pigpio.INPUT)
pi.set_mode(22, pigpio.INPUT)
#define functions and create variables that will count the triggers
count_one = 0
count_two = 0
count_three = 0
def ccone(gpio, level, tick):
    #functions will get values for gpio, level and tick from the callback
    #increasing the counter by 1
    
    global count_one
    count_one += 1

    global count_two
    
    if count_one == 10:

        count_one = 0
        
        count_two += 1
    
    if count_two == 50:
        
        count_two = 0
        
        global count_three
        count_three += 1
        
    #getting the current time and generating complete id
    timestamp = datetime.datetime.now()
    together = "{}.{}.{} - {}".format(count_three, count_two, count_one, timestamp)
    #putting the data into a .json format by creating a library and using json.dumps()
    python_shot_id = {
        "10Hz counter": count_one,
        "1Hz counter": count_two,
        "1/50Hz counter": count_three,
        "timestamp": "{}".format(timestamp),
        "ShotID": together
        }
    shot_id = json.dumps(python_shot_id, indent=5)
    print(shot_id)
    with open("ShotID.json", "w") as outfile:
        outfile.write(shot_id)
#setting up a callback that resolves cc_one everytime GPIO 5 detects a rising edge
cb1 = pi.callback(5, pigpio.RISING_EDGE, ccone)
#counters for other GPIOs can be added in the same way
time.sleep(timer)
cb1.cancel()
print("terminated")

